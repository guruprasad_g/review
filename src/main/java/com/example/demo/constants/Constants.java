package com.example.demo.constants;

public interface Constants {
	//Success codes
	public static final String SUCCESS_CODE    = "200";
    public static final String SUCCESS      = "SUCCESS";
    public static final String ERROR_CODE    = "400";
    public static final String ERROR      = "ERROR";
	
}

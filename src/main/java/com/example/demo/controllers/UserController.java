package com.example.demo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.DTOs.UserDTO;
import com.example.demo.response.Response;
import com.example.demo.service.UserService;

@CrossOrigin
@RestController
public class UserController {
	
	@Autowired
	UserService userService;

	@PostMapping("/userController/createUser")
	public ResponseEntity<Response> createUser(@RequestBody List<UserDTO> userDTOs) {
		return userService.createUsers(userDTOs);
	}
	
//	@PostMapping("/userController/searchUser")
//	public List<UserDTO> searchUser(@RequestBody UserDTO userDTO) {
//		return userService.searchUsers(userDTO);
//	}
	
//	@PutMapping("/userController/updateUser")
//	public ResponseEntity<Response> updateUser(@RequestBody UserDTO userDTO) {
//		return userService.updateUser(userDTO);
//	}
}

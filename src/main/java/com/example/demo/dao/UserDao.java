package com.example.demo.dao;

import java.util.List;

import com.example.demo.DTOs.UserDTO;
import com.example.demo.models.UserModel;

public interface UserDao {

	public UserModel findUserByUserEmail(String email);
	
	public UserModel findUserByUserName(String userName);
	
	public UserModel createOrUpdateUser(UserModel userModel);
	
	public UserModel findUserByUserId(Long userId);
	
	public List<UserDTO> searchUsers(UserDTO userDTO);
}

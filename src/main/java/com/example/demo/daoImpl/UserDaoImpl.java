package com.example.demo.daoImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.demo.DTOs.UserDTO;
import com.example.demo.dao.UserDao;
import com.example.demo.models.UserModel;
import com.example.demo.repository.UserRepository;
import com.example.demo.util.CommonUtil;

@Repository
public class UserDaoImpl implements UserDao{
	
	@PersistenceContext
	EntityManager entityManager;

	@Autowired
	UserRepository userRepository;
	
	@Override
	public UserModel findUserByUserEmail(String email) {
		return userRepository.findByUserEmail(email);
	}
	
	@Override
	public UserModel findUserByUserName(String userName) {
		return userRepository.findByUserName(userName);
	}
	
	@Override
	public UserModel createOrUpdateUser(UserModel userModel) {
		return userRepository.save(userModel);
	}

	@Override
	public UserModel findUserByUserId(Long userId) {
		return userRepository.findByIdUser(userId);
	}

	@Override
	public List<UserDTO> searchUsers(UserDTO userDTO) {

		List<UserDTO> userDTOs = new ArrayList<>();
		
		StringBuffer query = new StringBuffer();
		query.append("select u.id_user,u.user_email,u.user_name,u.active,u.active_from,");
		query.append(" u.active_to,ucr.user_name as createUserName ,ucu.user_name as updatedUserName,c.name as companyName,u.role_id,u.id_company");
		query.append(" from optimeyesai.optimeyesai_users u");
		query.append(" join optimeyesai.optimeyesai_users ucr on ucr.id_user = u.created_by");
		query.append(" join optimeyesai.optimeyesai_users ucu on ucu.id_user = u.updated_by");
		query.append(" join optimeyesai.optimeyesai_company c on c.id_company = u.id_company");
		query.append(" where (1=1) ");
		if(!CommonUtil.isNull(userDTO.getUserEmail())){
			query.append(" AND u.user_email like :userMail");
		}if(!CommonUtil.isNull(userDTO.getUserName())){
			query.append(" AND u.user_name like :userName");
		}if(!CommonUtil.isNull(userDTO.getActive())){
			query.append(" AND u.active like :activeFlag");
		}if(!CommonUtil.isNull(userDTO.getActiveFrom())&&CommonUtil.isNull(userDTO.getActiveTo())){
			query.append(" AND u.active_from between :startDate AND :startDate");
		}else if(CommonUtil.isNull(userDTO.getActiveFrom())&&!CommonUtil.isNull(userDTO.getActiveTo())){
			query.append(" AND u.active_to between :finishDate AND :finishDate");
		}else if(!CommonUtil.isNull(userDTO.getActiveFrom())&&!CommonUtil.isNull(userDTO.getActiveTo())){
			query.append(" AND u.active_from between :startDate AND :startDate");
			query.append(" AND u.active_to between :finishDate AND :finishDate");
		}
		
		Query qry  = entityManager.createNativeQuery(query.toString());
		
		if(!CommonUtil.isNullAndEmpty(userDTO.getUserEmail())){
			qry.setParameter("userMail",(CommonUtil.isNullAndEmpty(userDTO.getUserEmail())? "%%" : "%"+userDTO.getUserEmail()+"%"));
		}if(!CommonUtil.isNullAndEmpty(userDTO.getUserName())){
			qry.setParameter("userName",(CommonUtil.isNullAndEmpty(userDTO.getUserName())? "%%" : "%"+userDTO.getUserName()+"%"));
		}if(!CommonUtil.isNull(userDTO.getActive())){
			qry.setParameter("activeFlag",(CommonUtil.isNull(userDTO.getActive())? "%%" : "%"+userDTO.getActive()+"%"));
		}if(!CommonUtil.isNull(userDTO.getActiveFrom())&&CommonUtil.isNull(userDTO.getActiveTo())){
			qry.setParameter("startDate",userDTO.getActiveFrom());
		}else if(CommonUtil.isNull(userDTO.getActiveFrom())&&!CommonUtil.isNull(userDTO.getActiveTo())){
			qry.setParameter("finishDate",userDTO.getActiveTo());
		}else if(!CommonUtil.isNull(userDTO.getActiveFrom())&&!CommonUtil.isNull(userDTO.getActiveTo())){
			qry.setParameter("startDate",userDTO.getActiveFrom());
			qry.setParameter("finishDate",userDTO.getActiveTo());
		}
		
		List<Object> list = qry.getResultList();
		if (CommonUtil.isListNotNullAndEmpty(list)) {
			Iterator it = list.iterator();
			while (it.hasNext()){
				try {
					Object[] objs = (Object[]) it.next();
					UserDTO dto = new UserDTO();
					dto.setIdUser(Long.valueOf((Integer)objs[0]));
 				    dto.setUserEmail((String)objs[1]);
 				    dto.setUserName((String)objs[2]);
					dto.setActive((Character)objs[3]);
					dto.setActiveFrom(CommonUtil.toTimeStamp(CommonUtil.toDateStringFormat((Date)objs[4], "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss"));
					dto.setActiveTo(CommonUtil.toTimeStamp(CommonUtil.toDateStringFormat((Date)objs[5], "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss"));
					dto.setCreatedName((String)objs[6]);
					dto.setUpdatedName((String)objs[7]);
					dto.setCompanyName((String)objs[8]);
					// dto.setRoleId((String)objs[9]);
					dto.setIdCompany(Long.valueOf((Integer)objs[10]));
					userDTOs.add(dto);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return userDTOs;
	}
}

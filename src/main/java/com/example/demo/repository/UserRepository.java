package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.models.UserModel;

public interface UserRepository extends CrudRepository<UserModel, Long>{

	public UserModel findByUserEmail(String email);
	
//	public UserModel findByUserName(String userName);
	
	public UserModel findByIdUser(Long userId);

	public UserModel findByUserName(String name);
}

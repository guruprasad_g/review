package com.example.demo.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.example.demo.DTOs.UserDTO;
import com.example.demo.response.Response;

public interface UserService {

	public ResponseEntity<Response> createUsers(List<UserDTO> userDTOs); 
	
	public ResponseEntity<Response> updateUser(UserDTO userDTO); 
	
	public List<UserDTO> searchUsers(UserDTO userDTO);
	
}

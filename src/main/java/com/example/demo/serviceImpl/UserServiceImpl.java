package com.example.demo.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.demo.DTOs.UserDTO;
import com.example.demo.constants.Constants;
import com.example.demo.dao.UserDao;
import com.example.demo.models.UserModel;
import com.example.demo.response.Response;
import com.example.demo.service.UserService;
import com.example.demo.util.CommonUtil;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserDao userDao;


	@Override
	public ResponseEntity<Response> createUsers(List<UserDTO> userDTOs) {
		Response response = new Response();
		boolean createFlag = false;
		UserModel userModel = new UserModel();
		try {
			for (UserDTO dto : userDTOs) {
				if (!CommonUtil.isNullAndEmpty(dto.getUserEmail()) && !CommonUtil.isNullAndEmpty(dto.getUserName())
						&& !CommonUtil.isNullAndEmpty(dto.getPassword())
						&& CommonUtil.isListNotNullAndEmpty(dto.getRoleId()) && !CommonUtil.isNull(dto.getIdCompany())
						&& !CommonUtil.isNull(dto.getActive()) && !CommonUtil.isNull(dto.getActiveFrom())) {

					// Check if user mail already exits for company
					UserDTO userDtoUserMail = new UserDTO();
					userDtoUserMail.setUserEmail(dto.getUserEmail());
					List<UserDTO> userListUserMail = userDao.searchUsers(userDtoUserMail);

					if (CommonUtil.isListNotNullAndEmpty(userListUserMail)) {
						response.setResponseCode(Constants.ERROR_CODE);
						response.setResponseType(Constants.ERROR);
						response.setResponseMessage("User Mail " + dto.getUserEmail() + " already exits");
						return new ResponseEntity<Response>(response, HttpStatus.OK);
					}

					// Check if user name already exits for company
					UserDTO userDtoUserName = new UserDTO();
					userDtoUserName.setUserName(dto.getUserName());
					List<UserDTO> userListUserName = userDao.searchUsers(userDtoUserName);

					if (CommonUtil.isListNotNullAndEmpty(userListUserName)) {
						response.setResponseCode(Constants.ERROR_CODE);
						response.setResponseType(Constants.ERROR);
						response.setResponseMessage("User Name " + dto.getUserName() + " already exits");
						return new ResponseEntity<Response>(response, HttpStatus.OK);
					}

					userModel = CommonUtil.copyBeanProperties(dto, UserModel.class);


					// Check Active From for mandatory
					if (CommonUtil.isNull(userModel.getActiveFrom())) {
						response.setResponseCode(Constants.ERROR_CODE);
						response.setResponseType(Constants.ERROR);
						response.setResponseMessage("Active From is mandatory");
					}
					// Check Company is active
					userModel.setPassword(CommonUtil.encryptPassword(dto.getPassword()));
					userModel.setCreatedBy(-99L);
					userModel.setUpdatedBy(-99L);

					// check if company has active to then user active to
					// becomes mandatory
					
					createFlag = true;
					userDao.createOrUpdateUser(userModel);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			response.setResponseCode(Constants.ERROR_CODE);
			response.setResponseType(Constants.ERROR);
			response.setResponseMessage(e.getMessage());
			return new ResponseEntity<Response>(response, HttpStatus.OK);
		}

		if (createFlag) {
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseType(Constants.SUCCESS);
			response.setResponseMessage("User created successfully");
			return new ResponseEntity<Response>(response, HttpStatus.OK);
		} else {
			response.setResponseCode(Constants.ERROR_CODE);
			response.setResponseType(Constants.ERROR);
			response.setResponseMessage("No entry!! atleast one row to be filled");
			return new ResponseEntity<Response>(response, HttpStatus.OK);
		}
	}

	@Override
	public ResponseEntity<Response> updateUser(UserDTO userDTO) {
		Response response = new Response();
		UserModel userModel = new UserModel();
		try {
			userModel = userDao.findUserByUserId(userDTO.getIdUser());
			if (!userModel.getUserEmail().equals(userDTO.getUserEmail())) {
				// Check if user mail already exits for company
				UserDTO userDtoUserMail = new UserDTO();
				userDtoUserMail.setUserEmail(userDTO.getUserEmail());
				List<UserDTO> userListUserMail = userDao.searchUsers(userDtoUserMail);

				if (CommonUtil.isListNotNullAndEmpty(userListUserMail)) {
					response.setResponseCode(Constants.ERROR_CODE);
					response.setResponseType(Constants.ERROR);
					response.setResponseMessage("User Mail " + userDTO.getUserEmail() + " already exits");
					return new ResponseEntity<Response>(response, HttpStatus.OK);
				}
			}
			if (!userModel.getUserName().equals(userDTO.getUserName())) {
				// Check if user name already exits for company
				UserDTO userDtoUserName = new UserDTO();
				userDtoUserName.setUserName(userDTO.getUserName());
				List<UserDTO> userListUserName = userDao.searchUsers(userDtoUserName);

				if (CommonUtil.isListNotNullAndEmpty(userListUserName)) {

					response.setResponseCode(Constants.ERROR_CODE);
					response.setResponseType(Constants.ERROR);
					response.setResponseMessage("User Name " + userDTO.getUserName() + " already exits");
					return new ResponseEntity<Response>(response, HttpStatus.OK);
				}
			}
			userModel = CommonUtil.copyBeanProperties(userDTO, UserModel.class);

			userModel.setCreatedBy(-99L);
			userModel.setUpdatedBy(-99L);
			if (!CommonUtil.isNull(userModel.getActiveTo()) && !CommonUtil.isNull(userModel.getActiveFrom())
					&& userModel.getActiveTo().before(userModel.getActiveFrom())) {
				response.setResponseCode(Constants.ERROR_CODE);
				response.setResponseType(Constants.ERROR);
				response.setResponseMessage("Active To is before Active From");
				return new ResponseEntity<Response>(response, HttpStatus.OK);

			}
			userDao.createOrUpdateUser(userModel);
			response.setResponseCode(Constants.SUCCESS_CODE);
			response.setResponseType(Constants.SUCCESS);
			response.setResponseMessage("User updated successfully");
		} catch (Exception e) {
			e.printStackTrace();
			response.setResponseCode(Constants.ERROR_CODE);
			response.setResponseType(Constants.ERROR);
			response.setResponseMessage(e.getMessage());
			return new ResponseEntity<Response>(response, HttpStatus.OK);
		}
		return new ResponseEntity<Response>(response, HttpStatus.OK);
	}

	@Override
	public List<UserDTO> searchUsers(UserDTO userDTO) {
		if (!CommonUtil.isNull(userDTO.getActiveFrom())) {
			userDTO.setActiveFrom(CommonUtil.getStartDayTimestamp(
					CommonUtil.toDateStringFormat(userDTO.getActiveFrom(), "yyyy-MM-dd HH:mm:ss"),
					"yyyy-MM-dd HH:mm:ss"));
		}
		if (!CommonUtil.isNull(userDTO.getActiveTo())) {
			userDTO.setActiveTo(CommonUtil.getEndDayTimestamp(
					CommonUtil.toDateStringFormat(userDTO.getActiveTo(), "yyyy-MM-dd HH:mm:ss"),
					"yyyy-MM-dd HH:mm:ss"));
		}
		return userDao.searchUsers(userDTO);
	}

}

package com.example.demo.util;
import java.lang.reflect.Method;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.springframework.beans.BeanUtils;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class CommonUtil {

	
	private static MessageDigest md;
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-YYYY HH:mm:ss");
	//private static String ENDTIME = "23:59:59";
	//private static String STARTTIME = "00:00:00";

	public static boolean isNull(Object obj) {
		return obj == null;
	}

	public static boolean isNullAndEmpty(String data) {
		
		return (data == null || "".equals(data.trim()));
	}

	public static boolean isListNotNullAndEmpty(List list) {
		return (list != null && !list.isEmpty());
	}

	public static <Source, Destination> Destination copyBeanProperties(Source source, Class Destination) throws InstantiationException, IllegalAccessException{
		Destination destination = (Destination) Destination.newInstance();
		BeanUtils.copyProperties(source, destination);
		return destination;
	}
   

	public static String encryptPassword(String pass) {
		try {
			md = MessageDigest.getInstance("MD5");
			byte[] passBytes = pass.getBytes();
			md.reset();
			byte[] digested = md.digest(passBytes);
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < digested.length; i++) {
				sb.append(Integer.toHexString(0xff & digested[i]));
			}
			return sb.toString();
		} catch (NoSuchAlgorithmException ex) {
		}
		return null;

	}
	
	public static String generateOTP(int length) {
	    final String NUMBERS="123456789";
	    Random rnd = new Random();
	    StringBuilder sb = new StringBuilder(length);
	    for(int i = 0; i < length; i++) {
	    	sb.append(NUMBERS.charAt(rnd.nextInt(NUMBERS.length())));
	    }
	    return sb.toString();
	  }
	
	public static Timestamp getStartDayTimestamp(String date, String pattern) {
		try {
			 SimpleDateFormat dateFormat = new SimpleDateFormat();
			dateFormat.applyPattern(pattern);
			java.util.Date date2 = dateFormat.parse(date);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date2);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);

			return new Timestamp(calendar.getTimeInMillis());
		} catch (ParseException e) {
		}
		return null;
	}
	
	public static Timestamp getEndDayTimestamp(String date, String pattern) {
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat();
			dateFormat.applyPattern(pattern);
			java.util.Date date2 = dateFormat.parse(date);

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date2);
			calendar.set(Calendar.HOUR_OF_DAY, 23);
			calendar.set(Calendar.MINUTE, 59);
			calendar.set(Calendar.SECOND, 59);
			return new Timestamp(calendar.getTimeInMillis());
		} catch (ParseException e) {
		}
		return null;
	}

	public static Timestamp toTimeStamp(String date, String pattern){
		try{
			dateFormat.applyPattern(pattern);
			return new Timestamp(dateFormat.parse(date).getTime());
		}catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
	
	public static String toDateStringFormat(Timestamp date, String pattern) {
		if(null != date) {
			dateFormat.applyPattern(pattern);
			return dateFormat.format(date);
		}
		else {
			return null;
		}
	}
	
	public static String toDateStringFormat(Date date, String pattern) {
		if(null != date) {
			dateFormat.applyPattern(pattern);
			return dateFormat.format(date);
		}
		else {
			return null;
		}
	}
	
	public static Timestamp toTimeStampWithoutPattern(String date){
		try{
			return new Timestamp(dateFormat.parse(date).getTime());
		}catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
	
	public static Date toDate(Date date, String pattern) {
		dateFormat.applyPattern(pattern);
		return new Date(date.getTime());

	}
	
	public static Date toDate(String date, String pattern) {
		dateFormat.applyPattern(pattern);
		try {
			return dateFormat.parse(date);
		} catch (ParseException e) {
		}
		return null;
	}
	
	public static long calculateMonthsBetweenTwoDate(Date startDate, Date endDate) {
		Calendar startDateCal = Calendar.getInstance();
		startDateCal.setTime(startDate);
        Calendar endDateCal = Calendar.getInstance();
        endDateCal.setTime(endDate);
        
        int yearsInBetween = endDateCal.get(Calendar.YEAR)- startDateCal.get(Calendar.YEAR);
        int monthsDiff = endDateCal.get(Calendar.MONTH) - startDateCal.get(Calendar.MONTH);
        long ageInMonths = yearsInBetween*12 + monthsDiff;
        return ageInMonths;
	}
	
}
